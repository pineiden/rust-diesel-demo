#[macro_use]
extern crate diesel;
extern crate dotenv;
pub mod methods;

use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use std::env;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}


// schema and models objects

pub mod schema;
pub mod models;



// saving a new souce
use crate::models::{Source, NewSource};

pub fn create_source<'a>(
    conn: &PgConnection,
    source: &Source
)->  Result<Source, Box<dyn std::error::Error>>{
    use schema::sources;

    let new_source = NewSource {
        name: &source.name,
        code: &source.code,
        uri: &source.uri,
        active: &source.active,
        serie: &source.serie
    };

    match diesel::insert_into(sources::table)
        .values(new_source)
        .get_result(conn) {
            Ok(data) => return Ok(data),
            Err(err) => return Err(err.into())
        }
        
}


// saving a new souce
use crate::models::{Data, NewData, CsvData};

use crate::methods::get_source;


pub fn create_data<'a>(
    conn: &PgConnection,
    data: &CsvData
) ->  Result<Data, Box<dyn std::error::Error>>{
    /*
    take Data and save
     */
    use schema::dataset;
    let source = get_source(conn, &data.source).unwrap();
    let new_data = NewData {
        dt_gen: &data.dt_gen,
        data: &data.data,
        source_id: &source.id
    };
    
    match diesel::insert_into(dataset::table)
        .values(new_data)
        .get_result(conn) {
            Ok(data) => return Ok(data),
            Err(err) => return Err(err.into())
        }

}

