use serde::{Serialize, Deserialize};

#[derive(Debug, Identifiable, Queryable, Clone,Deserialize,Serialize)]
#[table_name="sources"]
pub struct Source {
    pub id: i32,
    pub name: String,
    pub code: String,
    pub uri: String,
    pub active: bool,
    pub serie: i32
}

use diesel::pg::PgConnection;

use crate::diesel::query_dsl::filter_dsl::FilterDsl;



use super::schema::sources;


#[derive(Insertable,Debug)]
#[table_name="sources"]
pub struct NewSource<'a> {
    pub name:&'a str,
    pub code:&'a str,
    pub uri:&'a str,
    pub active:&'a bool,
    pub serie:&'a i32
}



use chrono;
use chrono::prelude::*;
use serde_json;
use uuid::Uuid;

#[derive(Identifiable, Debug, Queryable, Clone, Associations, Serialize, Deserialize)]
#[belongs_to(Source)]
#[table_name="dataset"]
pub struct Data {
    pub id: Uuid,
    #[diesel(deserialize_as = "DtGen")]
    pub dt_gen: DateTime<Utc>,
    pub data: serde_json::Value,
    pub source_id: i32
}
/*
Ejemplo de consulta relacional:

http://docs.diesel.rs/diesel/associations/index.html
http://docs.diesel.rs/diesel/associations/trait.GroupedBy.html
use schema::{posts, users};

#[derive(Identifiable, Queryable, PartialEq, Debug)]
#[table_name = "users"]
pub struct User {
    id: i32,
    name: String,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(User)]
#[table_name = "posts"]
pub struct Post {
    id: i32,
    user_id: i32,
    title: String,
}

let user = users.find(2).get_result::<User>(&connection)?;
let users_post = Post::belonging_to(&user)
    .first(&connection)?;
let expected = Post { id: 3, user_id: 2, title: "My first post too".into() };
assert_eq!(expected, users_post);

 */


#[derive(Debug, Clone)]
pub struct DtGen(DateTime<Utc>);

impl Into<DateTime<Utc>> for DtGen {
    fn into(self) -> DateTime<Utc> {
        self.0
    }
}
use diesel::deserialize::Queryable;
use diesel::backend::Backend;

impl<DB, ST> Queryable<ST, DB> for DtGen
where
    DB: Backend,
    DateTime<Utc>: Queryable<ST, DB>,
{
    type Row = <DateTime<Utc> as Queryable<ST, DB>>::Row;

    fn build(row: Self::Row) -> Self {
        Self(<DateTime<Utc> as Queryable<ST, DB>>::build(row).with_timezone(&Utc))
    }
}


use super::schema::dataset;

#[derive(Insertable,Debug)]
#[table_name="dataset"]
pub struct NewData<'a> {
    pub dt_gen:&'a  DateTime<Utc>,
    pub data:&'a serde_json::Value,
    pub source_id:&'a i32
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CsvData {
    pub dt_gen: DateTime<Utc>,
    pub data:serde_json::Value,
    pub source: String
}



