use crate::models::Source;
use diesel::pg::PgConnection;

use crate::diesel::query_dsl::filter_dsl::FilterDsl;
use crate::diesel::RunQueryDsl;

use crate::diesel::ExpressionMethods;


pub fn get_source(
    conn: &PgConnection,
    source_code: &String) -> Result<Source, Box<(dyn std::error::Error + 'static)>> {
    use crate::schema::sources::dsl::{sources, code};
    match sources
        .filter(code.eq(source_code))
        .load::<Source>(conn) {
            Ok(mut results) => return Ok(results.pop().unwrap()),
            Err(err) => return Err(err.into())
        }
    
}
