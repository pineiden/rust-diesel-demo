extern crate diesel_demo;
extern crate diesel;

use diesel_demo::*;
use crate::models::{Source};
use diesel::prelude::*;

fn main() {
    use diesel_demo::schema::sources::dsl::*;
    let connection = establish_connection();
    let results = sources
        .limit(20)
        .load::<Source>(&connection)
        .expect("Error loading sources");

    println!("Mostrando {} sources", results.len());
    for source in results {
        println!("{}", source.name);
        println!("{}", source.code);
        println!("{}", source.uri);
        println!("--------------\n");
    }

}
