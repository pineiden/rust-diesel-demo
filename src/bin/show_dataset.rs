extern crate diesel_demo;
extern crate diesel;

use crate::schema::sources::dsl::{sources};

use diesel_demo::*;
use crate::models::{Data,Source};
use diesel::prelude::*;
use std::env::args;
//use diesel::expression_methods::PgArrayExpressionMethods;

fn main() {
    /* check length of args */
    let mut source_codes:Vec<String> = args().collect();
    source_codes.remove(0);
    println!("source codes {:?}", source_codes);
    let connection = establish_connection();
    let mut list_sources = vec![];
    
    if source_codes.len() > 0 {
        let capsule_source_codes:Vec<String> = source_codes.iter().map(|value|{
            format!("\'{}\'", value.to_uppercase())
        }).collect();
        let str_source_codes:String = capsule_source_codes.join(",");
        let filter = format!("code in ({})", str_source_codes);
        let results = sources
             .filter(diesel::dsl::sql(&filter))
             .load::<Source>(&connection)
             .expect("Error loading sources");
        for item in results {
            list_sources.push(item);
        }
    }
    else {
        println!("Loading all sources");
        let results = sources
           .load::<Source>(&connection)
           .expect("Error loading sources");
        for item in results {
            list_sources.push(item);
        }        
        }

    println!("Sources {}", list_sources.len());
    
    let results = Data::belonging_to(&list_sources)
        .load::<Data>(&connection)
        .unwrap()
        .grouped_by(&list_sources);

    let data = list_sources.into_iter().zip(results).collect::<Vec<_>>();

    println!("Mostrando {} dataset", data.len());
    
    for (ref source, ref dataset) in data.iter() {
        println!("{:?} - elems {}", source, dataset.len());
        for data in dataset.into_iter() {
            println!("{:?}, {}, {}", data.id, data.dt_gen,data.data);  
        } 
    }

}
