/*
Given a code, change the activation code

 */
extern crate diesel_demo;
extern crate diesel;

use self::diesel::prelude::*;
use self::diesel_demo::*;
use self::models::Source;
use std::env::args;

fn main() {
    use diesel_demo::schema::sources::dsl::*;
    let source_code = args()
        .nth(1)
        .expect("Toggle activation source require CODE");
    let connection = establish_connection();
    
    let pre_sources = sources.filter(code.eq(source_code.clone()))
        .load::<Source>(&connection)
        .expect("error on filter");
    
    for item in pre_sources {
        println!("item {:?}", item);

        let item_code = item.code;
        let result = diesel::delete(
            sources.filter(code.eq(item_code)))
            .execute(&connection)
            .expect("Error on deleting source");
        println!("item deleted {:?}", result);
    }

}

