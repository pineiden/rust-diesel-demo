extern crate diesel_demo;
extern crate diesel;


use self::diesel_demo::*;
use std::io::{stdin, Read};
use std::io;
use std::error::Error;
use std::process;
use self::models::{Data,CsvData};

/*
read from a csv file and save to database

 */

fn read_csv() -> Result<(), Box<dyn Error>> {
    let connection = establish_connection();
    let mut rdr = csv::Reader::from_reader(io::stdin());
    for result in rdr.deserialize() {
        let data: CsvData = result?;
        match create_data(&connection, &data){
            Ok(new_source) => println!("{:?}", new_source),
            Err(err) => println!("Error de escritura {}", err)
        };
        
    }
    
    Ok(())
}

fn main()  {
    if let Err(err) = read_csv() {
        println!("Error running example: {}", err);
        process::exit(1);
    }
}


#[cfg(not(windows))]
const EOF: &'static str = "CTRL+D";

#[cfg(windows)]
const EOF: &'static str = "CTRL+Z";
