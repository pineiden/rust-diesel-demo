/*
Given a code, change the activation code

 */
extern crate diesel_demo;
extern crate diesel;

use self::diesel::prelude::*;
use self::diesel_demo::*;
use self::models::Data;
use std::env::args;
use uuid::Uuid;

fn main() {
    use diesel_demo::schema::dataset::dsl::*;
    let mut dataset_ids:Vec<String> = args().collect();
    dataset_ids.remove(0);    
    let connection = establish_connection();

    for did in dataset_ids.into_iter() {
        let this_id = Uuid::parse_str(&did).unwrap();
        println!("{:?}", this_id);
        let result = diesel::delete(
            dataset.filter(id.eq(this_id)))
            .execute(&connection)
            .expect("Error on deleting source");
    }

}

