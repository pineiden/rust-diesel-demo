table! {
    _sqlx_migrations (version) {
        version -> Int8,
        description -> Text,
        installed_on -> Timestamptz,
        success -> Bool,
        checksum -> Bytea,
        execution_time -> Int8,
    }
}

table! {
    dataset (id) {
        id -> Uuid,
        dt_gen -> Timestamptz,
        data -> Jsonb,
        source_id -> Int4,
    }
}

table! {
    sources (id) {
        id -> Int4,
        name -> Varchar,
        code -> Varchar,
        uri -> Varchar,
        active -> Bool,
        serie -> Int4,
    }
}

table! {
    spatial_ref_sys (srid) {
        srid -> Int4,
        auth_name -> Nullable<Varchar>,
        auth_srid -> Nullable<Int4>,
        srtext -> Nullable<Varchar>,
        proj4text -> Nullable<Varchar>,
    }
}

table! {
    us_gaz (id) {
        id -> Int4,
        seq -> Nullable<Int4>,
        word -> Nullable<Text>,
        stdword -> Nullable<Text>,
        token -> Nullable<Int4>,
        is_custom -> Bool,
    }
}

table! {
    us_lex (id) {
        id -> Int4,
        seq -> Nullable<Int4>,
        word -> Nullable<Text>,
        stdword -> Nullable<Text>,
        token -> Nullable<Int4>,
        is_custom -> Bool,
    }
}

table! {
    us_rules (id) {
        id -> Int4,
        rule -> Nullable<Text>,
        is_custom -> Bool,
    }
}

joinable!(dataset -> sources (source_id));

allow_tables_to_appear_in_same_query!(
    _sqlx_migrations,
    dataset,
    sources,
    spatial_ref_sys,
    us_gaz,
    us_lex,
    us_rules,
);
