-- Your SQL goes here
create table source (
  id serial primary key, -- id
  name varchar(64) not null, -- name of source
  code varchar(12) not null, -- code
  uri varchar(512) not null, -- uri path to connect to source
  active boolean not null default 't', -- if active use this
  serie int not null CONSTRAINT positive_serie check (0<serie and serie < 65353) -- positive integer
)
