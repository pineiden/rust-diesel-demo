-- Your SQL goes here
create table dataset (
  id uuid DEFAULT uuid_generate_v1mc() primary key, -- id
  dt_gen timestamptz,
  data jsonb not null,
  source_id int,
  CONSTRAINT fk_source
    FOREIGN KEY(source_id)
    REFERENCES source(id)
    ON DELETE RESTRICT
)
