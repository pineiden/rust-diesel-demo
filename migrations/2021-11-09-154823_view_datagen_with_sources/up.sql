-- Your SQL goes here

create view dataset_with_sources as
  select dataset.id, dataset.dt_gen, dataset.data, sc.code, sc.name
           from dataset
                  inner join sources as sc
                               on dataset.source_id = sc.id
